<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
    <div class="container px-5">
        <a class="navbar-brand fw-bold"><img class="app-badge" src="assets/img/logo.png" alt="..." /></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="bi-list"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                <li class="nav-item"><a class="nav-link me-lg-3" href="#home"><b>Home</b></a></li>
                <li class="nav-item"><a class="nav-link me-lg-3" href="#jadwal"><b>Jadwal</b></a></li>
                <li class="nav-item"><a class="nav-link me-lg-3" href="#lokasi"><b>Lokasi</b></a></li>
                <li class="nav-item"><a class="nav-link me-lg-3" href="#info"><b>Info</b></a></li>
            </ul>
                <a href="//api.whatsapp.com/send?phone=088222340678&amp" class="button-kontak text-black" type="button" target="_blank"><b>Kontak</b></a>
        </div>
    </div>
</nav>
<!-- Mashead header-->
{{-- <header class="masthead bg-white" id="home">
    <div class="container px-5">
        <div class="row gx-5 align-items-center">
            <div class="col-lg-6">
                <!-- Mashead text and app badges-->
                <div class="mb-5 mb-lg-0 text-center text-lg-start">
                    <h1 class="display-1 lh-1 mb-3">Vaksin Sekarang dan Cegah Penyebaran COVID 19</h1>
                    {{-- <p class="lead fw-normal text-muted mb-5">Vaksinasi adalah salah satu usaha dari Pemerintah untuk menekan positif covid 19 di Indonesia. Ayo Vaksin Sekarang, <b>GRATIS<b>!</p> --}}
                    {{-- <div class="d-flex flex-column flex-lg-row align-items-center">
                        <a class="button-daftar text-black lead-inov me-lg-3 mb-4 mb-lg-0" href="https://docs.google.com/forms/d/e/1FAIpQLSc94hceIAvprvnd31B5tHO2oioFo6OQwLfFjYziXALdTZHjiQ/viewform" type="button" aria-label="Left Align" target="_blank"> Daftar Vaksinasi <img src="assets/img/arrow.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- Masthead device mockup feature-->
                <div class="masthead-device-mockup">
                    <div>
                        <img src="assets/img/daftar.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</header> --}} --}}

<header id="home" class="masthead bg-white">
    <div class="container px-5">
        <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
            <div class="col-12 col-lg-6">
                <h2 class="display-4 mb-4 text-black"><b>Vaksin Sekarang dan Cegah Penyebaran COVID 19</b></h2>
                <p class="lead fw-normal text-muted mb-5 mb-lg-0" style="line-height: 2em;">Vaksinasi merupakan salah satu dari Pemerintah untuk menekan positif covid 19 di Indonesia. Ayo Vaksin Sekarang, <b>GRATIS!</b></p>
                <br>
                <div class="d-flex flex-column flex-lg-row align-items-center">
                    <a class="button-daftar text-black lead-inov me-lg-3 mb-4 mb-lg-0" href="https://docs.google.com/forms/d/e/1FAIpQLSc94hceIAvprvnd31B5tHO2oioFo6OQwLfFjYziXALdTZHjiQ/viewform" type="button" aria-label="Left Align" target="_blank"> Daftar Vaksinasi <img src="assets/img/arrow.png" alt=""></a>
                </div>
            </div>
            <br>
            <div class="col-sm-8 col-md-6 col-12">
                {{-- <div class="px-5 px-sm-0 col-12 text-center"> --}}
                    <img src="assets/img/daftar.png" alt="..." />
                {{-- </div> --}}
            </div>
        </div>
    </div>
</header>