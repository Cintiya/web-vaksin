 <!--Jadwal Gebyar Vaksinasi STT Bandung-->
 <aside class="text-center bg-white" id="jadwal">
    <div class="container px-5">
        <div class="row gx-5 justify-content-center">
            <div class="col-xl-8">
                <div class="h2 text-black mb-2"><b>Jadwal Gebyar Vaksinasi STT Bandung</b></div>
                <p class="text-muted" style="line-height: 2em;">Gebyar Vaksinasi STT Bandung bersama TNI Kodim 0618/BS Kota Bandung dilaksanakan pada tanggal 24 Agustus 2021 dengan dibagi menjadi beberapa sesi.</p>
                <img src="assets/img/tnw-logo.svg" alt="..." style="height: 3rem" />
            </div>
        </div>
    </div>
</aside>

<!-- Lokasi Gebyar Vaksinasi STT Bandung-->
<section id="lokasi" >
    <div class="container px-5">
        <div class="row gx-5 align-items-center  justify-content-center justify-content-lg-between">
            <div class="col-lg-8 order-lg-1 mb-5 mb-lg-0 col-12">
                   <p class="display-4 mb-4 text-black"><b>Lokasi Gebyar Vaksinasi <br> STT Bandung</b></p>
                   <p class="lead fw-normal text-muted mb-5 mb-lg-0" style="line-height: 2em;">Gebyar Vaksinasi STT Bandung TNI Kodim 0618/BS Kota Bandung ini akan <b> dilaksanakan di Kampus Sekolah Tinggi Teknologi Bandung, Jl. Soekarno-Hatta No.378 Bandung.</b></p>
            </div>
            <div class="col-lg-4 order-lg-0 col-12">
                {{-- <div class="col-lg-6"> --}}
                    <!-- Masthead device mockup feature-->
                    <div class="masthead-device-mockup">
                        <div>
                            <img src="assets/img/layer2.png" alt="">
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>

<!-- Jenis Vaksin Gebyar Vaksinasi STT Bandung-->
<section id="info">
    <div class="container px-5">
        <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
            <div class="col-lg-8 mb-5 mb-lg-0 col-12">
                <h2 class="display-4 mb-4 text-black"><b>Jenis Vaksin Gebyar Vaksinasi <br> STT Bandung</b></h2>
                <p class="lead fw-normal text-muted mb-5 mb-lg-0" style="line-height: 2em;">Vaksin yang diberikan merupakan vaksin dosis pertama dengan jenis <b>Vaksin Sinovac-CoronaVac.</b></p>
            </div>
            <div class="col-lg-4 col-12">
                {{-- <div class="px-5 px-sm-0 col-12 text-center"> --}}
                    <img src="assets/img/layer3.png" alt="..." />
                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>

<!-- Peserta Gebyar Vaksinasi STT Bandung-->
<section>
    <div class="container px-5">
        <div class="row gx-5 align-items-center justify-content-center justify-content-lg-between">
            <div class="col-lg-8 order-lg-1 mb-5 mb-lg-0 col-12">
                   <p class="display-4 mb-4 text-black"><b>Peserta Gebyar Vaksinasi <br> STT Bandung</b></p>
                   <p class="lead fw-normal text-muted mb-5 mb-lg-0" style="line-height: 2em;">Vaksin ini <b>ditujukan bagi Dosen, Mahasiswa & Alumni beserta Keluarga dan Masyarakat dengan kuota per sesi 50 orang.</b></p>
            </div>
            <div class="col-lg-4 order-lg-0 col-12">
                {{-- <div class="col-lg-6"> --}}
                    <!-- Masthead device mockup feature-->
                    <div class="masthead-device-mockup">
                        <div>
                            <img src="assets/img/layer4.png" alt="">
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>

<!-- From Pendaftaran-->
<section class="bg-white" id="download">
    <div class="container px-5">
        <h2 class="text-center text-black font-alt mb-4">Form Pendaftaran</h2>
        <div class="d-flex flex-column flex-lg-row align-items-center justify-content-center">
            <a class="me-lg-3 mb-4 mb-lg-0" href="#!"><img class="app-badge" src="assets/img/google-play-badge.svg" alt="..." /></a>
            <a href="#!"><img class="app-badge" src="assets/img/app-store-badge.svg" alt="..." /></a>
        </div>
    </div>
</section>