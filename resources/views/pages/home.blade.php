<!DOCTYPE html>
<html lang="en">

    <head>
        @include('includes.meta')
        
        @include('includes.style')
    </head>

    <body id="page-top">
        @include('includes.header')

        @include('includes.body')
        
        @include('includes.footer')

        @include('includes.style')
    </body>

</html>
